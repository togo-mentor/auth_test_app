import React, { useEffect, useState } from 'react';
import './App.css';
import { useAuth0 } from "@auth0/auth0-react";
import axios from 'axios';

// 追加
interface Post {
  title: string,
  caption: string
}

function App() {
  const { isAuthenticated,loginWithRedirect,logout,getAccessTokenSilently } = useAuth0(); // 必要な機能をインポート
  //追加
  const [token, setToken] = useState<string>('')
  // 追加
  const [posts, setPosts] = useState<Post[]>()

  // 追加  
  useEffect(() => {
    const getToken = async () => {
      try {
        const accessToken = await getAccessTokenSilently({})
        setToken(accessToken)
      } catch (e: any) {
        console.log(e.message)
      }
    }
    getToken()
  })

  // 追加
  const fetchPosts = () => {
    axios.get('http://localhost:3000/api/v1/posts')
    .then((res) => {
      setPosts(res.data)
    })
  }

  // 追加
  const createPosts = () => {
    const headers = {
      headers: {
        Authorization: token,
        'Content-Type': 'application/json',
      }
    }
    const data = {
      title: 'タイトル2',
      caption: '説明2'
    }
    axios.post('http://localhost:3000/api/v1/posts',data,headers)
  }

  return (
    <div className="App">
      <div style={{padding:'20px'}}>
        <h2>ログインボタン</h2>
          <button onClick={() => loginWithRedirect()}>ログイン</button>
        <h2>ログアウトボタン</h2>
          <button onClick={() => logout()}>ログアウト</button>
        <h2>ログイン状態</h2>
        {
          isAuthenticated ?
          <p>ログイン</p>
          :
          <p>ログアウト</p>
        }
        <h2>投稿作成</h2>
          <button onClick={createPosts}>投稿作成</button>
        <h2>投稿一覧</h2>
          <button onClick={fetchPosts}>投稿取得</button>
          {posts?.map((post :Post,index:number) =>
          <div key={index}>
            <p>{post.title}</p>
            <p>{post.caption}</p>
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
